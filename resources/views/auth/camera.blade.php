@extends('layouts.app')

@section('content')

<head>
    <title>Capture webcam image with php and jquery - ItSolutionStuff.com</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script type="text/javascript" src="webcamjs-master/webcam.min.js"></script>
    <style>
        #my_camera{
         width: 320px;
         height: 240px;
         border: 1px solid black;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/2c7a93b259.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <!-- Webcam.min.js -->
</head>
<body >
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                <form enctype="multipart/form-data" method="POST" action="{{url('/upload')}}">
                    <div class="container" id="cameradiv" style="display: none;">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div id="my_camera"></div>
    <!--                                     <input type=button value="Take Snapshot" onClick="take_snapshot()">
    -->                                    <br/>
                                
                                <input type="hidden" name="image" class="image-tag">

                            </div>
                            <div class="col-md-6">
                                <div id="results">Your captured image will appear here...</div>
                            </div>
                        </div>
                    </div>
                    <input type="file" accept="image/*" id="file" style="display: none"/>
                    <input type="hidden" id="file_name"/>
                </form>
                    <div id="loggedin" >
                        <p>you are successfully logged in...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Configure a few settings and attach camera -->
<script language="JavaScript">
    window.onload = function(){
        navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);

        navigator.getMedia({video: true}, function() {
            Webcam.set({
                width: 320,
                height: 240,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach( '#my_camera' );
            setTimeout(function take_snapshot() {
                Webcam.snap( function(data_uri) {
                // display results in page
                document.getElementById('results').innerHTML = 
                '<img id="imagesform" src="'+data_uri+'"/>';
                } );
            },1000);

            setTimeout(function save() {
                var form_data = new FormData();
                var imagesform = document.getElementById("imagesform").src;

                form_data.append('file', imagesform);
                //alert($("#file").val());
                form_data.append('_token', '{{csrf_token()}}');
                //alert(form_data);
                $.ajax({
                    url: "{{url('upload')}}",
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {

                    Webcam.reset( '#my_camera' );  
                    alert('done');
                    
                });
            },3000);


        }, 
        function() {
            $.ajax({
                url: "{{url('blockcam')}}",
                data: form_data,
                type: 'GET',
                contentType: false,
                processData: false,
                success: function (data) { 
                alert('na');
                
            });
        });
    };
</script>

</body>
@endsection
