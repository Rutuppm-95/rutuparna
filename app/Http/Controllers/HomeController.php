<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Session;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    //save the webcam captured image to folder and the path to db
    public function upload(Request $request)
    {
        $userid = Auth::user()->id;
        $image = base64_decode($request['file']);
        $folderName = 'public/uploads/';
        $safeName = Str::random(10).'.'.'png';
        $destinationPath = public_path() . $folderName;
        $success = file_put_contents(public_path().'/uploads/'.$safeName, $image);

        //dd($destinationPath.$safeName);
        $finduser = User::find($userid);
        $finduser->image_path = $folderName.$safeName;
        $finduser->save();

        return $success;

    }
    //if camera access blocked then logout with message
    public function blockcam()
    {
        Auth::logout();
        Session::put('message','You need to allow camera access to login.');
        return redirect('/login');
    }
}
